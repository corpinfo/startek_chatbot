"""
This sample demonstrates an implementation of an fulfillment lambda for a fallback intent in Lex.

To use this Lambda function, you will need a Dynamo DB table with the name 'tickets'. Steps to create the table:
1. Go to Dynamo DB in AWS console and and click on Create table
2. Put the Table name as 'tickets' and primary key as 'ticket_number'
3. Click on create
"""

import json
import os
import logging
import boto3
import random
from time import gmtime, strftime
from datetime import datetime
from io import StringIO
import urllib.request
import pandas as pd
from boto3.dynamodb.conditions import Key, Attr

bucket = 'idealbot-transcripts'
dynamodb = boto3.resource('dynamodb')
locations_table = dynamodb.Table('Locations')
response_table = dynamodb.Table('Idealbot_Responses')

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

team_emails = {'realtions': 'ir-idealbot@startek.com', 'hr': 'hr-idealbot@startek.com', 'sales': 'sales-idealbot@startek.com', 'dialogue': 'id-idealbot@startek.com', 'ps': 'it-idealbot@startek.com', 'general': 'it-idealbot@startek.com'}

# --- Helpers that build all of the responses to Lex ---
def send_email(email_from, email_to, subject, message):
    boto3.client('ses', 'us-east-1').send_email(
        Source = email_from,
        Destination={
            'ToAddresses': [
                email_to,
            ]
        },
        Message={
            'Subject': {
                'Data': subject
            },
            'Body': {
                'Text': {
                    'Data': message
                }
            }
        }
    )


def append_transcript(output_session_attributes, source, message, intent_name, slots):
    transcript = []
    if 'transcript' in output_session_attributes:
        transcript = json.loads(output_session_attributes['transcript'])
    transcript.append(
        {
            "Participant": source,
            "Text": message,
            "Intent Name": intent_name,
            #"Slots": slots,
            "Timestamp": datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }
    )
    response = json.dumps(transcript, separators=(',', ':'))
    return response


def elicit_slot(session_attributes, intent_name, slots, slot_to_elicit, message):
    session_attributes['transcript'] = append_transcript(session_attributes, 'Lex', message['content'], intent_name, slots)
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitSlot',
            'intentName': intent_name,
            'slots': slots,
            'slotToElicit': slot_to_elicit,
            'message': message
        }
    }

def elicit_slot_card(session_attributes, intent_name, slots, slot_to_elicit, message, response_card):
    session_attributes['transcript'] = append_transcript(session_attributes, 'Lex', message['content'], intent_name, slots)
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitSlot',
            'intentName': intent_name,
            'slots': slots,
            'slotToElicit': slot_to_elicit,
            'message': message,
            'responseCard': response_card
        }
    }

def elicit_slot_card_ccpa(session_attributes, intent_name, slots, slot_to_elicit, message, response_card):
    session_attributes['transcript'] = append_transcript(session_attributes, 'Lex', 'CCPA Warning Here', intent_name, slots)
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitSlot',
            'intentName': intent_name,
            'slots': slots,
            'slotToElicit': slot_to_elicit,
            'message': message,
            'responseCard': response_card
        }
    }

def elicit_intent(session_attributes, message):
    session_attributes['transcript'] = append_transcript(session_attributes, 'Lex', message['content'], 'elicit_intent', 'none')
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': message
        }
    }

def elicit_intent_directions(session_attributes, message):
    session_attributes['transcript'] = append_transcript(session_attributes, 'Lex', 'Directions Place Holder', 'elicit_intent', 'none')
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': message
        }
    }

def elicit_intent_card(session_attributes, message, response_card):
    session_attributes['transcript'] = append_transcript(session_attributes, 'Lex', message['content'], 'elicit_intent', 'none')
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitIntent',
            'message': message,
            'responseCard': response_card
        }
    }

def confirm_intent(session_attributes, intent_name, slots, message):
    session_attributes['transcript'] = append_transcript(session_attributes, 'Lex', message['content'], intent_name, slots)
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intent_name,
            'slots': slots,
            'message': message
        }
    }

def confirm_intent_card(session_attributes, intent_name, slots, message, response_card):
    session_attributes['transcript'] = append_transcript(session_attributes, 'Lex', message['content'], intent_name, slots)
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intent_name,
            'slots': slots,
            'message': message,
            'responseCard': response_card
        }
    }

def close(session_attributes, fulfillment_state, message):
    session_attributes['transcript'] = append_transcript(session_attributes, 'Lex', message['content'], 'close', 'none')
    response = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': message
        }
    }

    return response

def delegate(session_attributes, slots):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Delegate',
            'slots': slots
        }
    }

def build_response_card(title, subtitle, options):
    """
    Build a responseCard with a title, subtitle, and an optional set of options which should be displayed as buttons.
    """
    buttons = None
    if options is not None:
        buttons = []
        for i in range(min(5, len(options))):
            buttons.append(options[i])

    return {
        'contentType': 'application/vnd.amazonaws.card.generic',
        'version': 1,
        'genericAttachments': [{
            'title': title,
            'subTitle': subtitle,
            'buttons': buttons
        }]
    }

# --- Helper Functions ---
def query_location(partition_key_value):
    """
    Return item read by primary key.
    """
    city_dict = {}
    response = locations_table.scan(
        FilterExpression= "contains(#address, :address)",
        ExpressionAttributeNames= {"#address": "Address",},
        ExpressionAttributeValues= {":address": partition_key_value,}  
    )
    for item in response['Items']:
        city_dict[item['Site_ID']] = {}
        city_dict[item['Site_ID']]['City'] = item['City']
        city_dict[item['Site_ID']]['Country'] = item['Country']
        city_dict[item['Site_ID']]['Latitude'] = str(item['Latitude'])
        city_dict[item['Site_ID']]['Longitude'] = str(item['Longitude'])

    return city_dict

def query_response(partition_key_value):
    resp = response_table.query(
        KeyConditionExpression=Key('   Response_Id').eq(partition_key_value)
    )
    for item in resp['Items']:
        if 'Response_1' not in item:
            text_response = item['Response_2']
        else:
            text_response = item['Response_1']
        additional_content = item['Additional_Content']
    
    return text_response, additional_content
def get_session_attributes(intent_request):
    """
    Get session attributes from intent request
    """
    return intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    
def get_user_id(intent_request):
    """
    Get user id from intent request
    """
    return intent_request['userId']
    
def get_input_transcript(intent_request):
    """
    Get input transcript from intent request
    """
    return intent_request['inputTranscript']
    
def create_ticket_entry(intent_request, input_transcript):
    
    ticket_number = str(random.randint(1,99999))
    #add_item(ticket_number, build_ticket_details(intent_request))
    
    return ticket_number
    
def build_ticket_details(intent_request, input_transcript):
    """
    Build ticket detail with user_id and input transcript
    """
    
    return json.dumps({
        'user_id': get_user_id(intent_request),
        'transcript': input_transcript
    })
# --- Intent handler ---

def handle_greeting(intent_request):
    """
    Performs fulfillment for the greeting intent
    """
    logger.debug('event.bot.name={}'.format(intent_request))
    output_session_attributes = get_session_attributes(intent_request)
    output_session_attributes['transcript'] = append_transcript(output_session_attributes, 'Customer', get_input_transcript(intent_request), intent_request['currentIntent']['name'], intent_request['currentIntent']['slots'])
    output_session_attributes['retry'] = 0
    slots = intent_request['currentIntent']['slots']
    firstname = intent_request['currentIntent']['slots']['firstname']

    if not firstname:
        return elicit_slot(
            output_session_attributes,
            intent_request['currentIntent']['name'],
            intent_request['currentIntent']['slots'],
            'firstname', 
            {
                'contentType': 'PlainText',
                'content': 'Hi, can you please tell me your first name?'
            }
        )
    
    if firstname:
        output_session_attributes['firstname'] = firstname
        return elicit_intent(
            output_session_attributes,
            {
                'contentType': 'PlainText',
                'content': 'Very nice meeting you, ' + firstname + ', I am Idealbot. What can I do for you?' 
            }
        )

    return delegate(output_session_attributes, slots)

def handle_directions(intent_request):
    """
    Performs fulfillment for the directions intent after persisting the missed utterance
    in a DynamoDB table.
    """
    endpoint = 'https://maps.googleapis.com/maps/api/directions/json?'
    api_key = 'AIzaSyAr6-R3tFGrGY59kCwmakAA4XVORp3TEN4'
    logger.debug('event.bot.name={}'.format(intent_request))
    output_session_attributes = get_session_attributes(intent_request)
    output_session_attributes['transcript'] = append_transcript(output_session_attributes, 'Customer', get_input_transcript(intent_request), intent_request['currentIntent']['name'], intent_request['currentIntent']['slots'])
    output_session_attributes['retry'] = 0
    city = intent_request['currentIntent']['slots']['city']
    origin = intent_request['currentIntent']['slots']['origin']
    
    # dynamoDB query for city

    if not city:
        return elicit_slot(
            output_session_attributes,
            intent_request['currentIntent']['name'],
            intent_request['currentIntent']['slots'],
            'city', 
            {
                'contentType': 'PlainText',
                'content': 'Please specify the desired office you need directions to.'
            }
        )
        
    if city and not origin:
        locations = query_location(city)
        if not locations:
            return elicit_slot(
                output_session_attributes,
                intent_request['currentIntent']['name'],
                intent_request['currentIntent']['slots'],
                'city', 
                {
                    'contentType': 'PlainText',
                    'content': "I'm sorry, but " + city + ' is not a city where we have a prescence. Please try another city.'
                }
            )
        else:
        # if len(locations) > 1:
        #     options = []
        #     while len(options) < 5:
        #         for i in locations:
        #             options.append({'text' : locations[i]['City'] + ', ' + locations[i]['Country'], 'value': ','.join((locations[i]['Latitude'],locations[i]['Longitude'])) })
        #     return elicit_slot_card(
        #         output_session_attributes,
        #         intent_request['currentIntent']['name'],
        #         intent_request['currentIntent']['slots'],
        #         'latlong', 
        #         {
        #             'contentType': 'PlainText',
        #             'content': 'Please choose an option:'
        #         },
        #         build_response_card(
        #             'Specify Appointment Type', 'What type of appointment would you like to schedule?',
        #             options
        #         )
        #     )
        # if len(locations) = 1:
        #     return elicit_slot(
        #         output_session_attributes,
        #         intent_request['currentIntent']['name'],
        #         intent_request['currentIntent']['slots'],
        #         'city', 
        #         {
        #             'contentType': 'PlainText',
        #             'content': "I'm sorry, but " + city + ' is not a city where we have a prescence. Please try another city.'
        #         }
        #     )
            limit = 0
            for x in locations:
                lat_long = ','.join((locations[x]['Latitude'],locations[x]['Longitude']))
                limit = limit + 1
                if limit == 1:
                    break
            output_session_attributes['latlong'] = lat_long
            return elicit_slot(
                output_session_attributes,
                intent_request['currentIntent']['name'],
                intent_request['currentIntent']['slots'],
                'origin', 
                {
                    'contentType': 'PlainText',
                    'content': 'Please enter your current address for directions to ' + city + '.'
                }
            )

    else:
        # google maps API location return"
        destination = output_session_attributes['latlong']
        nav_request = 'origin={}&destination={}&key={}'.format(origin.replace(' ','+'),destination,api_key)
        request = endpoint + nav_request
        response = urllib.request.urlopen(request).read()
        directions = json.loads(response)
        status = directions['status']
        if status == 'ZERO_RESULTS':
            intent_request['currentIntent']['slots']['city'] = ''
            intent_request['currentIntent']['slots']['origin'] = ''
            return elicit_slot(
                output_session_attributes,
                intent_request['currentIntent']['name'],
                intent_request['currentIntent']['slots'],
                'city', 
                {
                    'contentType': 'PlainText',
                    'content': "I'm sorry, but I couldn't find a route from " + origin + " to " + destination + ", please enter a new destination location."
                }
            )
        routes = directions['routes']
        route = routes[0]
        instructions = ""
        for i in route["legs"]:
            for g in i["steps"]:
                instructions = instructions + g["html_instructions"] + '\n'
        return elicit_intent_directions(
            output_session_attributes,
            {
                'contentType': 'PlainText',
                'content': 'Here are the instructions to ' + city + ':\n' + instructions
            }
        )

def help(intent_request):
    """
    Performs fulfillment for the Help intent.
    """
    session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}
    session_attributes['transcript'] = append_transcript(session_attributes, 'Customer', get_input_transcript(intent_request), intent_request['currentIntent']['name'], intent_request['currentIntent']['slots'])
    help_content = "My pleasure, happy to help. To provide the best option for you, let me ask you a question, Which of the following best captures your area of interest?  Investor, client, employment, or bot tester, none of the above, or I just want someone to contact me."
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': help_content
        }
    )

def handle_close(intent_request):
    """
    Performs fulfillment for the RegionDetails
    """
    logger.debug('event.bot.name={}'.format(intent_request))
    output_session_attributes = get_session_attributes(intent_request)
    output_session_attributes['transcript'] = append_transcript(output_session_attributes, 'Customer', get_input_transcript(intent_request), intent_request['currentIntent']['name'], intent_request['currentIntent']['slots'])
    firstname = intent_request['sessionAttributes']['firstname']
    return close(
        output_session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content': 'Okay, ' + firstname + ', have a wonderful day!'
        }
    )

def handle_fallback(intent_request):
    """
    Performs fulfillment for the fallback intent after persisting the missed utterance
    in a DynamoDB table.
    """
    logger.debug('event.bot.name={}'.format(intent_request))
    output_session_attributes = get_session_attributes(intent_request)
    output_session_attributes['transcript'] = append_transcript(output_session_attributes, 'Customer', get_input_transcript(intent_request), intent_request['currentIntent']['name'], intent_request['currentIntent']['slots'])
    output_session_attributes['missed_utterence'] = get_input_transcript(intent_request)       
    # return elicit_slot(
    #     output_session_attributes,
    #     'HumanContact',
    #     {'ticketNumber': ticket_number},
    #     'email_addr', 
    #     {
    #         'contentType': 'PlainText',
    #         'content': "I'm Sorry, " + firstname + ", I am not sure I understand. If you like, we can have someone get back to you regarding your questions??"
    #     }
    # )
    if 'retry' not in output_session_attributes or int(output_session_attributes['retry']) == 0:
        output_session_attributes['retry'] = 1
        return elicit_intent(
            output_session_attributes,
            {
                'contentType': 'PlainText',
                'content': "I'm Sorry, I am not sure I understand. I currently support WelcomeGreeting, IdealBotHelp, CorporateCCLocationsMAPS, and HumanContact. How can I help?"
            }
        )
    elif int(output_session_attributes['retry']) < 2:
        output_session_attributes['retry'] = int(output_session_attributes['retry']) + 1
        return elicit_intent(
            output_session_attributes,
            {
                'contentType': 'PlainText',
                'content': "I am very sorry, but I still don't understand. I currently support WelcomeGreeting, IdealBotHelp, CorporateCCLocationsMAPS, and HumanContact. How can I help?"
            }
        )
    else:
        options = [
            { "text":"WelcomeGreeting", "value":"Hello" },
            { "text":"IdealBotHelp", "value":"Help" },
            { "text":"Directions", "value":"Directions" },
            { "text":"HumanContact", "value":"Human" }
        ]
        #output_session_attributes['retry'] = 0
        return elicit_intent_card(
            output_session_attributes,
            {
                'contentType': 'PlainText',
                'content': "I am very sorry, but I still don't understand. Please choose and option below:"
            },
            build_response_card(
                'Specify Action',
                'What type of actions would you like to choose?',
                options
            )
        )
        # return confirm_intent(
        #     output_session_attributes,
        #     'HumanContact',
        #     intent_request['currentIntent']['slots'],
        #     {
        #         'contentType': 'PlainText',
        #         'content': "I'm Sorry, I am not sure I understand. Would you like to have someone get back to you regarding your questions?"
        #     }
        # )

def handle_communication_goals(intent_request):
    """
    Performs fulfillment for the RegionDetails
    """
    logger.debug('event.bot.name={}'.format(intent_request))
    output_session_attributes = get_session_attributes(intent_request)
    output_session_attributes['transcript'] = append_transcript(output_session_attributes, 'Customer', get_input_transcript(intent_request), intent_request['currentIntent']['name'], intent_request['currentIntent']['slots'])
    output_session_attributes['retry'] = 0
    status = intent_request['currentIntent']['confirmationStatus']
    confirmation = intent_request['currentIntent']['slots']['confirmation']
    if not confirmation:
        options = [
            { "text":"Yes", "value":"Yes" },
            { "text":"No", "value":"No" }
        ]
        return elicit_slot_card(
            output_session_attributes,
            intent_request['currentIntent']['name'],
            intent_request['currentIntent']['slots'],
            'confirmation',
            {
                'contentType': 'PlainText',
                'content': "Ideal Dialogue is a scientific process based on the social sciences. Do you have a background in the social sciences?"
            },
            build_response_card(
                'Please choose',
                'Yes or No',
                options
            )
        )
    elif confirmation == 'no':
        text_response, additional_content = query_response('ID_CG1_R1')
        if status == 'None':
            options = [
                { "text":"Yes", "value":"Yes" },
                { "text":"No", "value":"No" }
            ]
            return confirm_intent_card(
                output_session_attributes,
                intent_request['currentIntent']['name'],
                intent_request['currentIntent']['slots'],
                {
                    'contentType': 'PlainText',
                    'content': 'No problem, a background in Social Sciences is not necessary to understand Ideal Dialogue ' + text_response + ', would you like to know more?'
                },
                build_response_card(
                    'Please choose',
                    'Yes or No',
                    options
                )
            )
        elif status == "Denied":
            return close(
                output_session_attributes, 
                'Fulfilled',
                { 
                    'contentType': 'PlainText',
                    'content': 'No problem! Thank you.'
                }
            )
        elif status == "Confirmed":
            return close(
                output_session_attributes, 
                'Fulfilled',
                { 
                    'contentType': 'PlainText', 
                    'content': 'Here is Additional Content: ' + additional_content
                }
            )
    elif confirmation == 'yes':
        text_response, additional_content = query_response('ID_CG2_R1')
        if status == 'None':
            options = [
                { "text":"Yes", "value":"Yes" },
                { "text":"No", "value":"No" }
            ]
            return confirm_intent_card(
                output_session_attributes,
                intent_request['currentIntent']['name'],
                intent_request['currentIntent']['slots'],
                {
                    'contentType': 'PlainText',
                    'content': text_response + ' would you like to know more?'
                },
                build_response_card(
                    'Please choose',
                    'Yes or No',
                    options
                )
            )
        elif status == "Denied":
            return close(
                output_session_attributes, 
                'Fulfilled',
                { 
                    'contentType': 'PlainText',
                    'content': 'No problem! Thank you.'
                }
            )
        elif status == "Confirmed":
            return close(
                output_session_attributes, 
                'Fulfilled',
                { 
                    'contentType': 'PlainText', 
                    'content': 'Here is Additional Content: ' + additional_content
                }
            )

def handle_science(intent_request):
    """
    Performs fulfillment for the RegionDetails
    """
    logger.debug('event.bot.name={}'.format(intent_request))
    output_session_attributes = get_session_attributes(intent_request)
    output_session_attributes['transcript'] = append_transcript(output_session_attributes, 'Customer', get_input_transcript(intent_request), intent_request['currentIntent']['name'], intent_request['currentIntent']['slots'])
    output_session_attributes['retry'] = 0
    status = intent_request['currentIntent']['confirmationStatus']
    confirmation = intent_request['currentIntent']['slots']['confirmation']
    if not confirmation:
        options = [
            { "text":"Yes", "value":"Yes" },
            { "text":"No", "value":"No" }
        ]
        return elicit_slot_card(
            output_session_attributes,
            intent_request['currentIntent']['name'],
            intent_request['currentIntent']['slots'],
            'confirmation',
            {
                'contentType': 'PlainText',
                'content': "Ideal Dialogue is a scientific process based on the social sciences. Do you have a background in the social sciences?"
            },
            build_response_card(
                'Please choose',
                'Yes or No',
                options
            )
        )
    elif confirmation == 'no':
        text_response, additional_content = query_response('ID_S1_R3')
        if status == 'None':
            options = [
                { "text":"Yes", "value":"Yes" },
                { "text":"No", "value":"No" }
            ]
            return confirm_intent_card(
                output_session_attributes,
                intent_request['currentIntent']['name'],
                intent_request['currentIntent']['slots'],
                {
                    'contentType': 'PlainText',
                    'content': 'No problem, a background in Social Sciences is not necessary to understand Ideal Dialogue ' + text_response + ', would you like to know more?'
                },
                build_response_card(
                    'Please choose',
                    'Yes or No',
                    options
                )
            )
        elif status == "Denied":
            return close(
                output_session_attributes, 
                'Fulfilled',
                { 
                    'contentType': 'PlainText',
                    'content': 'No problem! Thank you.'
                }
            )
        elif status == "Confirmed":
            return close(
                output_session_attributes, 
                'Fulfilled',
                { 
                    'contentType': 'PlainText', 
                    'content': 'Here is Additional Content: ' + additional_content
                }
            )
    elif confirmation == 'yes':
        text_response, additional_content = query_response('ID_S2_R3')
        if status == 'None':
            options = [
                { "text":"Yes", "value":"Yes" },
                { "text":"No", "value":"No" }
            ]
            return confirm_intent_card(
                output_session_attributes,
                intent_request['currentIntent']['name'],
                intent_request['currentIntent']['slots'],
                {
                    'contentType': 'PlainText',
                    'content': text_response + ' would you like to know more?'
                },
                build_response_card(
                    'Please choose',
                    'Yes or No',
                    options
                )
            )
        elif status == "Denied":
            return close(
                output_session_attributes, 
                'Fulfilled',
                { 
                    'contentType': 'PlainText',
                    'content': 'No problem! Thank you.'
                }
            )
        elif status == "Confirmed":
            return close(
                output_session_attributes, 
                'Fulfilled',
                { 
                    'contentType': 'PlainText', 
                    'content': 'Here is Additional Content: ' + additional_content
                }
            )

def handle_contact_details(intent_request):
    """
    Performs fulfillment for the RegionDetails
    """
    logger.debug('event.bot.name={}'.format(intent_request))
    output_session_attributes = get_session_attributes(intent_request)
    output_session_attributes['transcript'] = append_transcript(output_session_attributes, 'Customer', get_input_transcript(intent_request), intent_request['currentIntent']['name'], intent_request['currentIntent']['slots'])
    output_session_attributes['retry'] = 0
    #missed_utterence = output_session_attributes['missed_utterence']
    ticket_number = str(random.randint(1,99999))
    #update_ticket_entry(ticket_number, email_id)
    email_id = intent_request['currentIntent']['slots']['email_addr']
    status = intent_request['currentIntent']['confirmationStatus']
    phone = intent_request['currentIntent']['slots']['phone']
    lastname = intent_request['currentIntent']['slots']['lastname']
    firstname = intent_request['currentIntent']['slots']['firstname']
    confirmation = intent_request['currentIntent']['slots']['confirmation']
    method = intent_request['currentIntent']['slots']['method']
    teams = intent_request['currentIntent']['slots']['teams']
    if not confirmation:
        options = [
            { "text":"Yes", "value":"Yes" },
            { "text":"No", "value":"No" }
        ]
        return elicit_slot_card_ccpa(
            output_session_attributes,
            intent_request['currentIntent']['name'],
            intent_request['currentIntent']['slots'],
            'confirmation',
            {
                'contentType': 'PlainText',
                'content': 'Based on the new California Consumer Protection Act (CCPA), we want to inform you if you wish to be contacted we will have to collect and save some of your personal data during this chat session. In order to stay CCPA compliant we need your consent to save your personal details. We only collect personal data related to your specific chat session and this data request is to help us with your identification in order to provide resolution or follow-up for your current request. It is our policy and practice to collect your first name, last name, email and/or phone, along with a transcript of the chat session. We use this information for the purpose of contacting you and to provide good customer service. We do not sell the information to any third parties. Do you consent to this data collection?'
            },
            build_response_card(
                'Please choose',
                'Yes or No',
                options
            )
        )
    elif confirmation == 'no':
        if teams:
            transcript = output_session_attributes['transcript']
            location = "0.00000000,0.00000000"
            ip_addr = "127.0.0.1"
            #ticket_details = build_ticket_details(intent_request, transcript)
            dataframe = {"Transcript": json.loads(transcript), "Team Email": team_emails.get(teams), "Location": location, "IP Address": ip_addr}
            df = pd.DataFrame([dataframe], columns=dataframe.keys())
            csv_buffer = StringIO()
            df.to_csv(csv_buffer)
            s3_resource = boto3.resource('s3')
            filename = ticket_number + '_' + datetime.now().strftime('%Y-%m-%d') + '.csv'
            s3_resource.Object(bucket, filename).put(Body=csv_buffer.getvalue())
            email_from = 'it-idealbot@startek.com'
            email_subject = 'IdealBot New Transcript'
            email_body = json.dumps(dataframe)
            send_email(email_from, team_emails.get(teams), email_subject, email_body)
            return close(
                output_session_attributes, 
                'Fulfilled',
                { 
                    'contentType': 'PlainText', 
                    'content': 'The email for the team requested is: ' + team_emails.get(teams) + '. Have a nice day'
                }
            )
            
        options = [
            { "text":"Sales", "value":"sales" },
            { "text":"Investor Relations", "value":"relations" },
            { "text":"HR", "value":"hr" },
            { "text":"Professional Services", "value":"ps" },
            { "text":"General Inquiries", "value":"general" }
        ]
        return elicit_slot_card(
            output_session_attributes,
            intent_request['currentIntent']['name'],
            intent_request['currentIntent']['slots'],
            'teams',
            {
                'contentType': 'PlainText',
                'content': 'Ok, if you prefer to contact us instead, for which Startek team would you like the email address?'
            },
            build_response_card(
                'Please choose',
                'Team contact info',
                options
            )
        )
    elif confirmation == 'yes':
        if teams:
            if not firstname:
                return elicit_slot(
                    output_session_attributes,
                    intent_request['currentIntent']['name'],
                    intent_request['currentIntent']['slots'],
                    'firstname',
                    {
                        'contentType': 'PlainText',
                        'content': 'What is your first name?'
                    }
                )

            if not lastname:
                return elicit_slot(
                    output_session_attributes,
                    intent_request['currentIntent']['name'],
                    intent_request['currentIntent']['slots'],
                    'lastname',
                    {
                        'contentType': 'PlainText',
                        'content': 'What is your last name?'
                    }
                )
            if not method:
                options = [
                    { "text":"Phone", "value":"phone" },
                    { "text":"Email", "value":"email" }
                ]
                return elicit_slot_card(
                    output_session_attributes,
                    intent_request['currentIntent']['name'],
                    intent_request['currentIntent']['slots'],
                    'method',
                    {
                        'contentType': 'PlainText',
                        'content': 'Thank you, let me get your contact details. What contact method should we use?'
                    },
                    build_response_card(
                        'Please choose',
                        'Contact method',
                        options
                    )
                )
            if method == 'phone' and not phone:
                return elicit_slot(
                    output_session_attributes,
                    intent_request['currentIntent']['name'],
                    intent_request['currentIntent']['slots'],
                    'phone',
                    {
                        'contentType': 'PlainText',
                        'content': 'What is your phone number?'
                    }
                )
            if method == 'email' and not email_id:
                return elicit_slot(
                    output_session_attributes,
                    intent_request['currentIntent']['name'],
                    intent_request['currentIntent']['slots'],
                    'email_addr', 
                    {
                        'contentType': 'PlainText',
                        'content': "Please enter your email address."
                    }
                )
            else:
                location = "0.00000000,0.00000000"
                ip_addr = "127.0.0.1"
                transcript = output_session_attributes['transcript']
                #ticket_details = build_ticket_details(intent_request, transcript)
                if method == 'phone':
                    dataframe = {"First Name": firstname, "Last Name": lastname, "Phone": phone, "Transcript": json.loads(transcript), "Team Email": team_emails.get(teams), "Location": location, "IP Address": ip_addr}
                elif method == 'email':
                    dataframe = {"First Name": firstname, "Last Name": lastname, "Email": email_id, "Transcript": json.loads(transcript), "Team Email": team_emails.get(teams), "Location": location, "IP Address": ip_addr}
                df = pd.DataFrame([dataframe], columns=dataframe.keys())
                csv_buffer = StringIO()
                df.to_csv(csv_buffer)
                s3_resource = boto3.resource('s3')
                filename = ticket_number + '_' + datetime.now().strftime('%Y-%m-%d') + '.csv'
                s3_resource.Object(bucket, filename).put(Body=csv_buffer.getvalue())
                email_from = 'it-idealbot@startek.com'
                email_subject = 'IdealBot New Support Request ID: ' + ticket_number + '_' + datetime.now().strftime('%Y-%m-%d')
                email_body = json.dumps(dataframe)
                send_email(email_from, team_emails.get(teams), email_subject, email_body)
                return close(
                    output_session_attributes, 
                    'Fulfilled',
                    { 
                        'contentType': 'PlainText', 
                        'content': 'Thank you for visiting with us today, a team member will be in contact with you shortly. Your ticket number for reference is ' + ticket_number + '_' + datetime.now().strftime('%Y-%m-%d')
                    }
                )
        options = [
            { "text":"Sales", "value":"sales" },
            { "text":"Investor Relations", "value":"relations" },
            { "text":"HR", "value":"hr" },
            { "text":"Professional Services", "value":"ps" },
            { "text":"General Inquiries", "value":"general" }
        ]
        return elicit_slot_card(
            output_session_attributes,
            intent_request['currentIntent']['name'],
            intent_request['currentIntent']['slots'],
            'teams',
            {
                'contentType': 'PlainText',
                'content': 'Thank you, which of our teams would you like to have contact you?'
            },
            build_response_card(
                'Please choose',
                'Team contact info',
                options
            )
        )

# --- Intents ---

def dispatch(intent_request):
    """
    Called when the user specifies an intent for this bot.
    """

    logger.debug('dispatch userId={}, intentName={}'.format(intent_request['userId'], intent_request['currentIntent']['name']))

    intent_name = intent_request['currentIntent']['name']
    output_session_attributes = get_session_attributes(intent_request)
    # Dispatch to your bot's intent handlers
    if intent_name == 'WelcomeGreeting':
        return handle_greeting(intent_request)
    elif intent_name == 'IdealBotHelp':
        return help(intent_request)
    elif intent_name == 'CloseGreeting':
        return handle_close(intent_request)
    elif intent_name == 'CorporateCCLocationsMAPS':
        return handle_directions(intent_request)
    elif intent_name == 'IdealBotFallback':
        return handle_fallback(intent_request)
    elif intent_name == 'HumanContact':
        return handle_contact_details(intent_request)
    elif intent_name == 'IdealDialogueCommGoals':
        return handle_communication_goals(intent_request)
    elif intent_name == 'IdealDialogueScience':
        return handle_science(intent_request)
    else:
        raise Exception('Intent with name ' + intent_name + ' not supported')


# --- Main handler ---


def lambda_handler(event, context):
    """
    Route the incoming request based on intent.
    The JSON body of the request is provided in the event slot.
    """
    logger.debug('event.bot.name={}'.format(event['bot']['name']))
    #os.environ['TZ'] = 'America/New_York'
    #time.tzset()

    return dispatch(event)